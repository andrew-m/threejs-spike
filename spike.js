
var setupScene = function () {
	// set the scene size
	var WIDTH = 400,
	  HEIGHT = 300;

	// set some camera attributes
	var VIEW_ANGLE = 45,
	  ASPECT = WIDTH / HEIGHT,
	  NEAR = 0.1,
	  FAR = 10000;

	// get the DOM element to attach to
	// - assume we've got jQuery to hand
	var $container = $('#target-div');

	// create a WebGL renderer, camera
	// and a scene
	var renderer = new THREE.WebGLRenderer();
	var camera =
	  new THREE.PerspectiveCamera(
	    VIEW_ANGLE,
	    ASPECT,
	    NEAR,
	    FAR);

	var scene = new THREE.Scene();

	// add the camera to the scene
	scene.add(camera);

	// the camera starts at 0,0,0
	// so pull it back
	camera.position.z = 300;

	// start the renderer
	renderer.setSize(WIDTH, HEIGHT);

	// attach the render-supplied DOM element
	$container.append(renderer.domElement);

	// set up the sphere vars
	var radius = 50,
	    segments = 16,
	    rings = 16;

    // create the sphere's material
	var greenMaterial =
	  new THREE.MeshLambertMaterial(
    {
      color: 0xaaFFaa
    });

	var earthTexture = THREE.ImageUtils.loadTexture( './images/earth.jpg' );

	var earthMaterial = new THREE.MeshLambertMaterial({ map: earthTexture }); 


	// create a new mesh with
	// sphere geometry - we will cover
	// the sphereMaterial next!
	var sphere = new THREE.Mesh(

	  new THREE.SphereGeometry(
	    radius,
	    segments,
	    rings),

	  earthMaterial);

	var cube = new THREE.Mesh(
		new THREE.CubeGeometry(
			50,
			50,
			50),
		greenMaterial);
	cube.position.set(101,0,50);

	var cubexrotation = 0;
	var cubeyrotation = 30;
	var cubezrotation = 10;
	cube.rotation.set(cubexrotation,cubeyrotation,cubezrotation);


	var spherexrotation = 0;
	var sphereyrotation = 0;
	var spherezrotation = 0;
	sphere.rotation.set(spherexrotation,sphereyrotation,spherezrotation);

	// add the sphere to the scene
	scene.add(sphere);
	scene.add(cube);

	// create a point light
	var pointLight =
	  new THREE.PointLight(0xFFFFFF);

	// var ambientLight = new THREE.AmbientLight(0x111111);

	// set its position
	pointLight.position.x = 300;
	pointLight.position.y = 300;
	pointLight.position.z = 300;

	// add to the scene
	scene.add(pointLight);
	// scene.add(ambientLight);
		
	renderer.render(scene, camera);

	var loopx = 0;
	var loopz = 1;

	setInterval(function(){
		// draw!
		renderer.render(scene, camera);
		// cubexrotation += 0.02;
		// cubeyrotation += 0.002;
		// cubezrotation += 0.005;

		sphereyrotation += 0.01;

		sphere.rotation.set(spherexrotation, sphereyrotation, spherezrotation);
		console.log(cube);
		// alert(cubexrotation);
	
		var oldx = cube.position.x;
		var oldz = cube.position.z;
		var theta = 0.001;
		cube.position.x = (oldx * Math.cos(theta)) - (oldz * Math.sin(theta));
		cube.position.z = (oldx * Math.sin(theta)) + (oldz * Math.cos(theta));


		var oldly = pointLight.position.y;
		var oldlx = pointLight.position.x;
		var theta = 0.01;
		pointLight.position.y = (oldly * Math.cos(theta)) - (oldlx * Math.sin(theta));
		pointLight.position.x = (oldly * Math.sin(theta)) + (oldlx * Math.cos(theta));



		// }, 1000
		cube.rotation.set(cubexrotation,cubeyrotation,cubezrotation);
		// cube.position.x = 
		// cube.rotation.set(cubexrotation,cubeyrotation,cubezrotation);
	}, 10
	)

}

$(document).ready(setupScene);
